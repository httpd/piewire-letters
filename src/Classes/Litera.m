//
//  Litera.m
//  Pipefire Letters!
//
//  Created by Maciej Rapacz on 12.01.2013.
//
//

#import "Litera.h"

@implementation Litera
@synthesize czas, znak;
-(NSString *)znak
{
    if(znak == nil)
    {
        znak = @"";
    }
    return znak;
}
- (id)init
{
    self = [super init];
    if (self) {
        czas = 0;
        text = [SPTextField textFieldWithText: self.znak];
        text.height = 20;
        self.height = 20;
        text.width = 20;
        text.color = 0x111111;
        [self addChild:text];
        [self addEventListener:@selector(touch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
    }
    return self;
}
+(Litera *)generujrandomowalitere
{
    Litera *l = [[Litera alloc] init];
    [l losuj];
    return l;
}
-(void)losuj
{
    self.znak = [NSString stringWithFormat:@"%c",arc4random()%25 + 65];
    text.text = self.znak;
}
-(void)touch: (SPTouchEvent*) event
{
    NSSet *set = [event touchesWithTarget:self andPhase:SPTouchPhaseBegan];
    if(set.count)
    {
        NSLog(@"%@",self.znak);
    }
}
@end

//
//  Rura.h
//  Pipefire Letters!
//
//  Created by Maciej Rapacz on 12.01.2013.
//
//

#import <Foundation/Foundation.h>
#import "Litera.h"
@interface Rura : SPDisplayObjectContainer
{
    NSArray *litery;
    double wysokoscznaku;
    SPTween *snapowanie;
    int poprzedniigrek;
    int startowypunktrury;
}
@property int iloscznakow;
- (id)initZLiterami:(int)liczba;

@end

//
//  Game.m
//  AppScaffold
//

#import "Game.h" 

// --- private interface ---------------------------------------------------------------------------

@interface Game ()

- (void)setup;
- (void)onImageTouched:(SPTouchEvent *)event;
- (void)onResize:(SPResizeEvent *)event;

@end


// --- class implementation ------------------------------------------------------------------------
//trolol spam
@implementation Game

@synthesize gameWidth  = mGameWidth;
@synthesize gameHeight = mGameHeight;

- (id)initWithWidth:(float)width height:(float)height
{
    if ((self = [super init]))
    {
        mGameWidth = width;
        mGameHeight = height;
        [self setup];
    }
    return self;
}

- (void)dealloc
{
    // release any resources here
    
    [Media releaseAtlas];
    [Media releaseSound];
    
}

- (void)setup
{
    
    [SPAudioEngine start];  // starts up the sound engine
    
    
    [Media initAtlas];      // loads your texture atlas -> see Media.h/Media.m
    [Media initSound];      // loads all your sounds    -> see Media.h/Media.m
    
    
    
    SPQuad *background = [[SPQuad alloc] initWithWidth:mGameWidth height:mGameHeight color:0xFFFFFF];
    [self addChild:background];
    NSLog(@"lololol");
    [self addEventListener:@selector(onResize:) atObject:self forType:SP_EVENT_TYPE_RESIZE];
    SPQuad *przycisk = [[SPQuad alloc] init];
    przycisk.y = 200;
    przycisk.x = 200;
    przycisk.width = 30;
    przycisk.height = 30;
    przycisk.color = 0x112233;
    
    [self addChild:przycisk];
    [przycisk addEventListener:@selector(dotkniecie:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
    // We release the objects, because we don't keep any reference to them.
    // (Their parent display objects will take care of them.)
    // 
    // However, if you don't want to bother with memory management, feel free to convert this
    // project to ARC (Automatic Reference Counting) by clicking on 
    // "Edit - Refactor - Convert to Objective-C ARC".
    // Those lines will then be removed from the project.
    
    
    
    // Per default, this project compiles as a universal application. To change that, enter the 
    // project info screen, and in the "Build"-tab, find the setting "Targeted device family".
    //
    // Now choose:  
    //   * iPhone      -> iPhone only App
    //   * iPad        -> iPad only App
    //   * iPhone/iPad -> Universal App  
    // 
    // To support the iPad, the minimum "iOS deployment target" is "iOS 3.2".
    
    rury = [NSArray array];
    [self zespawnuj];
}

- (void)onImageTouched:(SPTouchEvent *)event
{
    NSSet *touches = [event touchesWithTarget:self andPhase:SPTouchPhaseEnded];
    if ([touches anyObject])
    {
        [Media playSound:@"sound.caf"];
    }
}

- (void)onResize:(SPResizeEvent *)event
{
    NSLog(@"new size: %.0fx%.0f (%@)", event.width, event.height, 
          event.isPortrait ? @"portrait" : @"landscape");
}
- (void)dotkniecie:(SPTouchEvent *)event
{
    NSSet *set = [event touchesWithTarget:self andPhase:SPTouchPhaseBegan];
    if ([set anyObject])
    {
        [self zespawnuj];
            }
}
-(void)zespawnuj
{
    for (Rura *rura in rury)
    {
        [self removeChild:rura];
    }
    rury = [NSArray array];
    for (int i = 0; i < 4; i++)
    {
        Rura *rura = [[Rura alloc] initZLiterami:6];
        rury = [rury arrayByAddingObject:rura];
        rura.x += i*50;
        [self addChild:rura];
    }
}
@end

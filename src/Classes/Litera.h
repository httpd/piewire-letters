//
//  Litera.h
//  Pipefire Letters!
//
//  Created by Maciej Rapacz on 12.01.2013.
//
//

#import <Foundation/Foundation.h>

@interface Litera : SPDisplayObjectContainer
{
    SPTextField *text;
}
@property float czas;

@property (nonatomic, assign)NSString *znak;
+(Litera *)generujrandomowalitere;
@end

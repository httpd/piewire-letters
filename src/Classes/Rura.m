//
//  Rura.m
//  Pipefire Letters!
//
//  Created by Maciej Rapacz on 12.01.2013.
//
//

#import "Rura.h"

@implementation Rura
@synthesize iloscznakow;
- (id)init
{
    self = [super init];
    if (self) {
        self.iloscznakow = 0;
    }
    return self;
}
- (id)initZLiterami:(int)liczba
{
    self = [super init];
    if (self) {
        self.iloscznakow = liczba;
        litery = [NSArray array];
        for (int i = 0; i < liczba; i++) {
            litery = [litery arrayByAddingObject:[Litera generujrandomowalitere]];
            
        }
        [self setup];
        [self addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
    }
    return self;
}
-(void) setup
{
    self.x = 100;
    startowypunktrury = 40; // WAZNE PRZY SNAPOWANIU
    self.y = startowypunktrury;
    poprzedniigrek = self.y;
    int przesuniecie = 0;
    for (Litera *l in litery)
    {
        l.y += przesuniecie;
        wysokoscznaku = l.height;
        przesuniecie += wysokoscznaku;
        [self addChild:l];
    }
[self rozpocznijAnimacje:2];
}
-(void)rozpocznijAnimacje: (int)oIle
{
    SPTween *tween = [SPTween tweenWithTarget:self time:0.5 transition:SP_TRANSITION_EASE_IN_OUT];
    [tween animateProperty:@"y" targetValue:wysokoscznaku*oIle];
    [[SPStage mainStage].juggler addObject:tween];
}
-(void)onTouch:(SPTouchEvent *) event
{
    NSSet *set = [event touchesWithTarget:self andPhase:SPTouchPhaseMoved];
    if(set.count)
    {
        SPTouch *touch = [set anyObject];
        float roznica = touch.globalX - touch.previousGlobalX;
        self.y += roznica;
    }
    set = [event touchesWithTarget:self andPhase:SPTouchPhaseBegan];
    if(set.count)
    {
        [self.stage.juggler removeObject:snapowanie];
    }
    set = [event touchesWithTarget:self andPhase:SPTouchPhaseEnded];
    if(set.count)
    {
        snapowanie = [SPTween tweenWithTarget:self time:0.5 transition:SP_TRANSITION_EASE_OUT];
        float reszta, ostatnipelny;
        double zpm;
        zpm = wysokoscznaku;
        reszta = modf((self.y-startowypunktrury)/wysokoscznaku, &zpm);
        reszta = self.y -startowypunktrury - zpm*wysokoscznaku;
        ostatnipelny = self.y - reszta;
        float x = abs(poprzedniigrek - self.y);
        NSLog(@"%f", x);
        if(abs(poprzedniigrek - self.y) > wysokoscznaku /2 || abs(poprzedniigrek - self.y) < wysokoscznaku /5)
            if(reszta > wysokoscznaku/2)
            {
                [snapowanie animateProperty:@"y" targetValue:ostatnipelny + wysokoscznaku];
            }
            else
            {
                [snapowanie animateProperty:@"y" targetValue:ostatnipelny];
            }
        else
            if (poprzedniigrek - self.y > 0)
                [snapowanie animateProperty:@"y" targetValue:poprzedniigrek - wysokoscznaku];
            else
                [snapowanie animateProperty:@"y" targetValue:poprzedniigrek + wysokoscznaku];

        [self.stage.juggler addObject:snapowanie];
        [snapowanie addEventListener:@selector(koniecsnapowania:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    }
    
}
-(void)koniecsnapowania:(SPEvent *) argument
{
    poprzedniigrek = self.y;
    
}
@end
